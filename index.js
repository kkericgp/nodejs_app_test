const express = require('express')
const app = express()
const port = 3000

app.use(express.json());
app.use(express.urlencoded({extended:true}))
var session = require("express-session")
app.use(express.static("public"));
app.use(session({ secret: "cats" }));

app.get('/hello', function (req, res) { //Question 3a
  res.send('Hello World')
})

app.post('/sortnum', function (req, res) { //Question 3b; assuming data passed as JSON array format, with key 'numbers'
  try {
    const sorted_array = req.body.numbers
    sorted_array.sort(function (a, b) {  return a - b;  })
    console.log(sorted_array)
    res.send(JSON.stringify(sorted_array))    
  } catch (error) {
    console.error(error);
    res.send({"message": error.message})
  }
})



var passport = require('passport') //initialize authentication parameters
  , LocalStrategy = require('passport-local').Strategy;
app.use(passport.initialize());
app.use(passport.session());
var user = {
  username: 'admin',
  password: 'Admin&8181',
  id: 1
}
var jwt = require('jsonwebtoken');
var token = jwt.sign({ username: 'admin' }, 'A secret key'); // token signed with HMAC SHA256 (Default in jsonwebtoken library)

passport.serializeUser(function(user, done) {//serialize&deserialize user
  done(null, user.id);
});
passport.deserializeUser(function(id, done) {
    done(null, false);
});

passport.use(new LocalStrategy({ //Authentication process
    usernameField: 'login',
    passwordField: 'password'
  },
  function(username, password, done) {
    try {
      if (!(username===user.username)) {return done(null, false, { message: 'Incorrect username.' })}
      if (!(password===user.password)) {return done(null, false, { message: 'Incorrect password.' })}
      return done(null,user)
    } catch (error) {
      console.error(error);
      res.send({"message": error.message})
    }
  }
));

app.post('/login', //Question 3C; assuming keys 'login' and 'password' passed as JSON
  passport.authenticate('local'),
  function(req, res) {
    res.send(token)
  });

app.use(function (req, res, next) { //]404 responses
  res.status(404).send("This page doesn't exist.")
  })
   
app.listen(port, () => {
  console.log(`listening at http://localhost:${port}`)
})